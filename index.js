const express = require("express")
const app = express()

const port = 3000

app.use(express.json())

app.get("/", (req, res) => {
	res.json({ "page" : "home" })
})

app.get("/ping", (req, res) => {
	res.json({"ping" : "pong"})
})


app.listen(port, () => {
	console.log(`server started on port ${port}`)
})

