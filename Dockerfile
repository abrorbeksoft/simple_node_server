FROM node:20-alpine

RUN mkdir /home/app
WORKDIR /home/app

COPY package.json .
COPY index.js .

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]

